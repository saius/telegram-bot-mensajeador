#!/bin/bash

# Correr este script haciendo:
# source install-requirements.sh

# crear venv si no existe
[ ! -d venv ] && python3 -m venv venv

# si no está activado el venv activarlo
[ -z $VIRTUAL_ENV ] && source venv/bin/activate

# instalar requerimientos
pip install -r requirements.txt
