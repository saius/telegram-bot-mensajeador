import logging, sys
from pprint import pprint

# github - https://github.com/python-telegram-bot/python-telegram-bot
# code snippets - https://github.com/python-telegram-bot/python-telegram-bot/wiki/Code-snippets
# reference - https://python-telegram-bot.readthedocs.io/
import telegram
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, ConversationHandler
from dotenv import load_dotenv
load_dotenv(verbose=True)

from bot.env import try_get_multiple
from bot.db import db, recreate
from bot.job import add_msg_job
import bot.commands as commands

BOT_TOKEN, LOGGING = try_get_multiple('BOT_TOKEN, LOGGING')
LOGGING = LOGGING.lower() in ['1', 'true', 'yes', 'y']

# creamos la DB si no tiene data
if not db.get_tables():
    recreate(force=True)

# Enable logging
if LOGGING:
    import traceback
    # a mejorar siguiendo: https://github.com/python-telegram-bot/python-telegram-bot/wiki/Code-snippets#an-good-error-handler
    logging.basicConfig(format='%(asctime)s - %(levelname)s - %(name)s - %(message)s', level=logging.INFO)
    logger = logging.getLogger(__name__)
    def error(update, context):
        """Log Errors caused by Updates."""
        trace = "".join(traceback.format_tb(sys.exc_info()[2]))
        logger.error('Update %s caused error: %s. Traceback:\n%s', update.update_id if update else '-', context.error, trace)
        try: update.message.reply_text(f'📛 Ups, me crashié!')
        except Exception: pass

def main():
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater(BOT_TOKEN, use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    print(f'Login de bot correcto:\n{dp.bot.name} "{dp.bot.first_name}"')
    print(f'Link de bot updates:\n{dp.bot.base_url}/getUpdates')
    #pprint(dir(dp.bot))
    #pprint(vars(dp.bot))

    if LOGGING:
        # log all errors
        dp.add_error_handler(error)

    # on different commands - answer in Telegram
    #dp.add_handler(CommandHandler("help", help))
    for command_handler in commands.all_command_handlers:
        dp.add_handler(command_handler)

    # Start the Bot
    try:
      updater.start_polling()
    except telegram.error.Unauthorized as e:
      print('Api token inválido. Err:', str(e))
      sys.exit(1)

    # arrancamos el job
    add_msg_job(updater)

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    print('Bot corriendo')
    updater.idle()
    print('Bot cerrado')

if __name__ == '__main__':
    main()
