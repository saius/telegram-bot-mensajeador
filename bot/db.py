import datetime

from peewee import *

from bot.env import try_get

DB_FILE=try_get('DB_FILE')

# pragmas - https://docs.peewee-orm.com/en/latest/peewee/database.html#recommended-settings
db = SqliteDatabase(DB_FILE, pragmas={'journal_mode': 'wal'})

# Models and Fields - https://docs.peewee-orm.com/en/latest/peewee/models.html
class BaseModel(Model):
    class Meta:
        database = db

class Admin(BaseModel):
    username = CharField(unique=True)

class Mensaje(BaseModel):
    id = CharField(unique=True)
    seteado_por = ForeignKeyField(Admin)
    activo = BooleanField(default=False)
    grupo = TextField()
    # dias es 7 letras "1" o "0" según encendido apagado de domingo-a-sábado
    dias = FixedCharField(max_length=7, default='0'*7)
    horario = TimeField(default=datetime.time(0, 0, 0))
    last_sent_texto = IntegerField(default=99)

class Texto(BaseModel):
    # Relationships and Joins - https://peewee.readthedocs.io/en/latest/peewee/relationships.html
    # el backref es el campo que va a autocrearse en la tabla foránea de relación
    mensaje = ForeignKeyField(Mensaje, backref='textos')
    contenido = TextField()

DB_MODELS = [Admin, Mensaje, Texto]

MSJ_GUITA_ID = 'pedimo_guita'

def recreate(force=False):
    if not force and input('Estás segurx que querés re/crear la DB? [y/n] ').lower() != 'y':
        print('Cancelado')
        return
    print('Recreando DB')
    if db.is_closed():
        db.connect()
    db.drop_tables(DB_MODELS)
    db.create_tables(DB_MODELS)
    saio = Admin.create(username='saio_nara')
    Admin.create(username='Gulag4u')
    Admin.create(username='Lupint')
    Mensaje.create(
        id=MSJ_GUITA_ID,
        seteado_por=saio,
        texto='ayudis, necesitamos pasta',
        grupo='@grupo_de_testeo'
    )
    db.close()
    print('DB recreada')

def get_msg_guita():
    return Mensaje.get(Mensaje.id==MSJ_GUITA_ID)
