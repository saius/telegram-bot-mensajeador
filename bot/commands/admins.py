from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import (
    CommandHandler,
    CallbackQueryHandler,
    ConversationHandler,
    CallbackContext,
    MessageHandler,
    Filters,
)

from bot.constants import CONVERSATIONS, USERNAME_REGEX
from bot.db import Admin
from bot.job import restart_msg_job
from bot.utils import restricted
from .cancel import cancel

ADMINS, ADMINS_VIEW, ADMINS_ADD, ADMINS_DELETE = CONVERSATIONS.create_consts(4)
# estas son variables de los botones, NO de los states
# le ponemos namespace casero con "_"
VIEW, ADD, DELETE, RETURN, END, END_NOCLEAN = [f'{ADMINS}_{i}' for i in range(6)]

def create_handler():
    return ConversationHandler(
        entry_points=[CommandHandler('admins', buttons_command)],
        states={
            ADMINS: [
                CallbackQueryHandler(view, pattern='^' + VIEW + '$'),
                CallbackQueryHandler(start_add, pattern='^' + ADD + '$'),
                CallbackQueryHandler(start_delete, pattern='^' + DELETE + '$'),
            ],
            ADMINS_VIEW: [],
            ADMINS_ADD: [MessageHandler((Filters.text & ~Filters.command), add)],
            ADMINS_DELETE: [CallbackQueryHandler(delete, pattern='^@.+')],
        },
        fallbacks=[
            CallbackQueryHandler(buttons_command, pattern='^' + RETURN + '$'),
            CallbackQueryHandler(end, pattern='^' + END + '$'),
            CallbackQueryHandler(end_noclean, pattern='^' + END_NOCLEAN + '$'),
            CommandHandler('cancelar', cancel),
        ],
    )

def end(update, context):
    # CallbackQuery - https://python-telegram-bot.readthedocs.io/en/stable/telegram.callbackquery.html
    query = update.callback_query
    query.answer()
    query.edit_message_text('❇️ Operación finalizada')
    return ConversationHandler.END

def end_noclean(update, context):
    query = update.callback_query
    query.answer()
    # limpiamos los botones y dejamos todo como está
    query.edit_message_reply_markup(InlineKeyboardMarkup([]))
    return ConversationHandler.END

def ending_inline_opts(noclean=False):
    return [
        InlineKeyboardButton("<< Volver", callback_data=RETURN),
        InlineKeyboardButton("Terminar", callback_data=END_NOCLEAN if noclean else END),
    ]

@restricted
def buttons_command(update, context):
    # InlineKeyboardButton -  https://python-telegram-bot.readthedocs.io/en/stable/telegram.inlinekeyboardbutton.html
    keyboard = [
        [
            InlineKeyboardButton("Ver", callback_data=VIEW),
            InlineKeyboardButton("Agregar", callback_data=ADD),
            InlineKeyboardButton("Eliminar", callback_data=DELETE),
        ],
        [InlineKeyboardButton("Terminar", callback_data=END)],
    ]

    reply_markup = InlineKeyboardMarkup(keyboard)
    reply_text = '⏺ Qué deseás hacer con les admins?'

    # diferenciamos si viene por entry point o por un "volver"
    if update.message:
        update.message.reply_text(reply_text, reply_markup=reply_markup)
    else:
        query = update.callback_query
        query.answer()
        query.edit_message_text(reply_text, reply_markup=reply_markup)

    return ADMINS

@restricted
def view(update, context):
    keyboard = [ending_inline_opts(noclean=True)]
    reply_markup = InlineKeyboardMarkup(keyboard)

    admins = Admin.select()
    admins_str = '\n'.join([f'- @{a.username}' for a in admins])

    query = update.callback_query
    query.answer()
    query.edit_message_text(f'❇️ Les admins actualmente son:\n{admins_str}', reply_markup=reply_markup)

    return ADMINS_VIEW

@restricted
def start_add(update, context):
    query = update.callback_query
    query.answer()
    query.edit_message_text(f'⏺ Okey, mandame el nombre del usuarie (o cancelá con /cancelar):')

    return ADMINS_ADD

@restricted
def add(update, context):
    username = update.message.text
    if username[0] == '@':
        username = username[1:]

    if not username or not USERNAME_REGEX.match(username):
        update.message.reply_text('✴️ Nombre de usuarie inválido. Corregí y volvé a intentar:')
        return

    Admin.create(username=username)

    keyboard = [
        ending_inline_opts()
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)

    # devolvemos
    update.message.reply_text(f'✅ @{username} agregade como admin!', reply_markup=reply_markup)

    return CONVERSATIONS.FALLBACK

@restricted
def start_delete(update, context):
    admins = Admin.select()
    admins_usernames = [f'@{a.username}' for a in admins]

    keyboard = []
    for u in admins_usernames:
        keyboard.append([InlineKeyboardButton(u, callback_data=u)])
    keyboard.append(ending_inline_opts())

    reply_markup = InlineKeyboardMarkup(keyboard)

    query = update.callback_query
    query.answer()
    query.edit_message_text(f'❇️ A qué admin deaseás eliminar?', reply_markup=reply_markup)

    return ADMINS_DELETE

@restricted
def delete(update, context):
    query = update.callback_query
    query.answer()

    # en start_delete definimos la callback_data como @usu
    username = query.data[1:]
    Admin.get(Admin.username == username).delete_instance()

    keyboard = [ending_inline_opts()]
    reply_markup = InlineKeyboardMarkup(keyboard)

    query.edit_message_text(f'✅ Admin @{username} eliminade :/', reply_markup=reply_markup)

    return ADMINS_DELETE
