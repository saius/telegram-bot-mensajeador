from datetime import datetime, timezone

from telegram.ext import CommandHandler

from bot.db import get_msg_guita
from bot.job import get_msg_job
from bot.utils import restricted, secs_to_text

def create_handler():
    return CommandHandler("estado", command)

@restricted
def command(update, context):
    msg = get_msg_guita()

    if msg.dias == '0'*7:
        dias_status = 'ningún día'
    elif msg.dias == '1'*7:
        dias_status = 'todos los días'
    else:
        dias_letras = ['D', 'L', 'M', 'M', 'J', 'V', 'S']
        dias_states = [d for d in msg.dias]
        dias_status = ' '.join([dias_letras[i] if d == '0' else f'*{dias_letras[i]}*' for i, d in enumerate(dias_states)])

    # pasamos de int a str y ponemos un cero adelante si es de un solo caracter
    horario_str = f'{str(msg.horario.hour).rjust(2,"0")}:{str(msg.horario.minute).rjust(2,"0")}'

    if msg.textos:
        next_texto_i = 0 if msg.last_sent_texto >= len(msg.textos)-1 else msg.last_sent_texto+1
        next_texto = msg.textos[next_texto_i].contenido
    else:
        next_texto = '\-'

    # MarkdownV2 - https://stackoverflow.com/questions/62230148/python-telegram-bot-markdown
    grupo_escapeado = msg.grupo.replace("_", "\_").replace("-", "\-")
    reply = f'__Estado__: {"🟢 prendido" if msg.activo else "🔴 apagado"}\n' \
    f'__Grupo__: {grupo_escapeado}\n' \
    f'__Días activo__: {dias_status}\n' \
    f'__Horario__: {horario_str}\n' \
    f'__Textos configurados__: {len(msg.textos)}\n' \
    f'__Próximo mensaje a mandar__:\n{next_texto}'

    update.message.reply_text(reply, parse_mode='MarkdownV2')
