from telegram.ext import CommandHandler

from bot.db import Admin
from bot.commands import all_command_names

def create_handler():
    return CommandHandler("start", command)

def command(update, context):
    reply_text = 'Hola yo soy l@ RBot mensajeador@! 🤖\n'

    username = update.effective_user.username
    admins = [admin.username for admin in Admin.select()]
    if username not in admins:
        reply_text += '\nVeo que no estás en mi lista de admins, no vas a poder usarme. Si creés que esto es un error pedí tu permiso en el grupo del hacklab.'
        reply_text += '\n\nEstoy hech@ en Python, si te interesa este es mi código fuente:\nhttps://git.rlab.be/saionala/telegram-bot-mensajeador'
    else:
        reply_text += '\nPara ver todos los comandos disponibles apretá en el " / " cerca de tu botón de enviar mensaje.\n\n'
        reply_text += 'O clickealos de la siguiente lista:\n'
        reply_text += '\n'.join([f'- /{command_name}' for command_name in all_command_names])

    update.message.reply_text(reply_text, disable_web_page_preview=True)
