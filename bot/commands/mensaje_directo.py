import re

from telegram.ext import ConversationHandler, CommandHandler, MessageHandler, Filters, CallbackQueryHandler
from telegram import InlineKeyboardButton, InlineKeyboardMarkup

from bot.constants import CONVERSATIONS
from bot.db import get_msg_guita
from bot.utils import restricted
from .cancel import cancel

GET_TEXT, GET_URL = CONVERSATIONS.create_consts(2)
# estas son variables de los botones, NO de los states
# le ponemos namespace casero con "_"
URL_YES, URL_NO = [f'{GET_URL}_{i}' for i in range(2)]

def create_handler():
    # https://github.com/python-telegram-bot/python-telegram-bot/blob/master/examples/conversationbot.py
    # Add conversation handler
    return ConversationHandler(
        entry_points=[CommandHandler('mensaje_directo', entry_point)],
        states={
            GET_TEXT: [MessageHandler((Filters.text & ~Filters.command), get_text)],
            GET_URL:  [
                CallbackQueryHandler(url_yes, pattern='^' + URL_YES + '$'),
                CallbackQueryHandler(url_no, pattern='^' + URL_NO + '$'),
            ],
        },
        fallbacks=[CommandHandler('cancelar', cancel)],
    )

@restricted
def entry_point(update, context):
    update.message.reply_text(f'⏺ Okey, mandame el texto del mensaje que querés que envíe (o cancelá con /cancelar). ' \
    'Tené en cuenta que lo voy a mandar ni bien lo reciba.')

    return GET_TEXT

# cuando hay una url el markdown queda en el formato: [url.com](http://url.com)
reg_url_urled = re.compile('\[.+\]\(.+\)')
@restricted
def get_text(update, context):
    # si usamos text_markdown_v2 a secas no se arman previews de los links
    # text_markdown_v2 - https://python-telegram-bot.readthedocs.io/en/stable/telegram.message.html#telegram.Message.text_markdown_v2
    text = update.message.text_markdown_v2_urled
    context.chat_data['saved_message_text'] = text

    if len(reg_url_urled.findall(text)):
        # InlineKeyboardButton -  https://python-telegram-bot.readthedocs.io/en/stable/telegram.inlinekeyboardbutton.html
        keyboard = [
            [
                InlineKeyboardButton("Sí", callback_data=URL_YES),
                InlineKeyboardButton("No", callback_data=URL_NO),
            ],
        ]
        reply_markup = InlineKeyboardMarkup(keyboard)

        update.message.reply_text(f'⏺ Detecté un link en tu mensaje. Querés que incluya una vista previa del mismo?', reply_markup=reply_markup)

        return GET_URL
    else:
        return send_saved_message(update, context)

@restricted
def url_yes(update, context):
    return send_saved_message(update, context, disable_web_page_preview=False)
@restricted
def url_no(update, context):
    return send_saved_message(update, context, disable_web_page_preview=True)

def send_saved_message(update, context, disable_web_page_preview=False):
    text = context.chat_data['saved_message_text']

    msg = get_msg_guita()
    context.bot.send_message(chat_id=msg.grupo, text=text, parse_mode='MarkdownV2', disable_web_page_preview=disable_web_page_preview)

    if update.message:
        update.message.reply_text('✅ Enviado!')
    else:
        query = update.callback_query
        query.answer()
        query.edit_message_text('✅ Enviado!')

    return ConversationHandler.END
